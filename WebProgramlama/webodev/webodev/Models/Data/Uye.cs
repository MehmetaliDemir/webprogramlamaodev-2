﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace webodev.Data
{
    public class Uye
    {
        public int UyeId { get; set; }

        
        [Required(ErrorMessage = "Lütfen adınızı giriniz.")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Adınızı 3-50 karakter arasında girebilirsiniz.")]
        public string Ad { get; set; }

        [Required(ErrorMessage = "Lütfen soyadınızı giriniz.")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Soyadınızı 3-50 karakter arasında girebilirsiniz.")]
        public string Soyad { get; set; }

        [Required(ErrorMessage = "Lütfen e-posta adresinizi giriniz.")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Lütfen e-posta adresinizi geçerli bir formatta giriniz.")]
        public string EPosta { get; set; }

      

        //Bir üyenin, birden çok yorumu olabileceği için Yorumları bir liste içerisine alıyoruz. 
        //Property adının sonuna s takısı koymamızın sebebi, veri tipinin çoğul olduğunun daha kolay anlaşılabilmesi 
        //içindir. Entity varsayılan olarak, liste verilerin sonuna s takısı koymaktadır.
        //Biz de bu standarda uyum gösterdik. Eğer veri tipinin adı zaten s ile bitiyorsa o zaman da s yi siliyoruz.
        //Örneğin hayvanlarla ilgili bir tabloda, Kus adlı bir sınıfımız çoğul olarak temsil edilecekse Ku olarak 
        //kullanabiliriz.
        public virtual List<Yorum> Yorums { get; set; }

        //Bir üyenin eklediği, birden çok makale olabilir.
        public virtual List<Makale> Makales { get; set; }

    }
}