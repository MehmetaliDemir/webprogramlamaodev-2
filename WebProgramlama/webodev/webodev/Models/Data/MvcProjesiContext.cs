﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace webodev.Data
{
    public class MvcProjesiContext
    {
        SqlConnection connection = null;
        SqlCommand command = null;
        SqlDataReader reader = null;
        string connectionString = ConfigurationManager.ConnectionStrings["Spencers"].ConnectionString;

        //Get Categories
        public List<Uye> GetCategories()
        {
            List<Uye> categories = null;
            try
            {
                using (connection = new SqlConnection(connectionString))
                {
                    using (command = new SqlCommand("uspGetCategories", connection))
                    {
                        connection.Open();
                        if (connection.State == ConnectionState.Open)
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            reader = command.ExecuteReader();
                            categories = new List<Uye>();
                            while (reader.Read())
                            {
                                categories.Add(
                                    new Uye()
                                    {
                                        UyeId = int.Parse(reader["UyeId"].ToString()),
                                        Ad = reader["Ad"].ToString(),
                                        Soyad = reader["Soyad"].ToString(),
                                    }
                                    );
                            }
                        }
                    }
                }
                return categories;
            }
            catch (Exception)
            {

                throw;
            }
        }
    }

}
