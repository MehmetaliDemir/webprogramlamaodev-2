﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(webodev.Startup))]
namespace webodev
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
